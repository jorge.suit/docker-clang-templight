FROM ubuntu:16.04
LABEL maintainer="Jorge Suit <josp.jorge at gmail.com>"
LABEL description="A linux C++ build environment based on clang+templight"

RUN apt-get update && apt-get install -y \
  gcc \
  cmake \
  g++ \
  git \
  python \
  zlib1g-dev && \
  mkdir /src && \
  cd /src && \
  git clone --depth=1 https://git.llvm.org/git/llvm.git && \
  cd /src/llvm/tools && \
  git clone --depth=1 https://git.llvm.org/git/clang.git && \
  cd /src/llvm/tools/clang/tools && \
  git clone --depth=1 https://github.com/mikael-s-persson/templight.git && \
  echo "add_clang_subdirectory(templight)" >> CMakeLists.txt && \
  mkdir /src/llvm/build && \
  cd /src/llvm/build && \
  cmake .. -DCMAKE_BUILD_TYPE=Release && \
  make -j6 && make install && \
  apt-get install -y \
  libboost-filesystem-dev \
  libboost-graph-dev \
  libboost-program-options-dev \
  libboost-test-dev && \
  cd /src && \
  git clone --depth=1 https://github.com/mikael-s-persson/templight-tools.git && \
  cd templight-tools && \
  mkdir build && cd build && \
  cmake .. -DCMAKE_BUILD_TYPE=Release && \
  make -j6 && make install && \
  apt-get clean autoclean autoremove -y && \
  cd / && rm -rf /src